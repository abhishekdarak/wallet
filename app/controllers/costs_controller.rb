class CostsController < ApplicationController
  before_action :set_cost, only: [:show, :edit, :update, :destroy]

  # GET /costs
  # GET /costs.json
  def index
    @costs = current_user.costs
  end

  # GET /costs/1
  # GET /costs/1.json
  def show
  end

  # GET /costs/new
  def new
    @cost = Cost.new
  end

  # GET /costs/1/edit
  def edit
  end

  # POST /costs
  # POST /costs.json
  def create
    @cost = Cost.new(cost_params)
    @cost.user_id = current_user.id
    respond_to do |format|
      if @cost.save 
        if check_amount?(cost_params[:category_id])
          format.html { redirect_to @cost, flash:{ notice: 'Cost was successfully created.', error: 'you spending too much'}  }
          format.json { render :index, status: :created, location: @cost }
        else
          format.html { redirect_to @cost, notice: 'Cost was successfully created.' }
          format.json { render :index, status: :created, location: @cost }
        end

      else
        format.html { render :new }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /costs/1
  # PATCH/PUT /costs/1.json
  def update
    respond_to do |format|
      if @cost.update(cost_params)
        if check_amount?(cost_params[:category_id])
          format.html { redirect_to @cost, flash:{ notice: 'Cost was successfully created.', alert: 'you spending too much'}  }
          format.json { render :index, status: :created, location: @cost }
        else
          format.html { redirect_to @cost, notice: 'Cost was successfully created.' }
          format.json { render :index, status: :created, location: @cost }
        end

      else
        format.html { render :new }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /costs/1
  # DELETE /costs/1.json
  def destroy
    @cost.destroy
    respond_to do |format|
      format.html { redirect_to costs_url, notice: 'Cost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cost
      @cost = Cost.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cost_params
      params.require(:cost).permit(:name, :description, :amount, :category_id)
    end

    def check_amount?(id)
      total_costs = current_user.categories.find(id).costs.map { |cost| cost.amount }.reduce(0, :+)
      total_incomes = current_user.categories.find(id).incomes.map { |income| income.amount }.reduce(0, :+) 
      if total_costs > total_incomes
        true
      else
        false
      end
    end
end
