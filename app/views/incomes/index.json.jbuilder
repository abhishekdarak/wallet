json.array!(@incomes) do |income|
  json.extract! income, :id, :name, :description, :amount
  json.url income_url(income, format: :json)
end
