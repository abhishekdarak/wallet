class Cost < ActiveRecord::Base
  belongs_to :category, dependent: :destroy
  belongs_to :user, dependent: :destroy

  validates :amount,  numericality: { only_integer: true }

  extend FriendlyId
  friendly_id :name, use: :slugged
end
