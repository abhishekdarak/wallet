class Category < ActiveRecord::Base
  belongs_to :user
  has_many :costs
  has_many :incomes

  extend FriendlyId
  friendly_id :name, use: :slugged

  validates :name, presence: true
end
