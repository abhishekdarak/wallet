class AddCategoryIdToIncomes < ActiveRecord::Migration
  def change
    add_column :incomes, :category_id, :integer
  end
end