class AddSlugToCosts < ActiveRecord::Migration
  def change
    add_column :costs, :slug, :string
    add_index :costs, :slug, unique: true
  end
end
