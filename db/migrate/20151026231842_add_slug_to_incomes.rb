class AddSlugToIncomes < ActiveRecord::Migration
  def change
    add_column :incomes, :slug, :string
    add_index :incomes, :slug, unique: true
  end
end
