class AddCategoryIdToCosts < ActiveRecord::Migration
  def change
    add_column :costs, :category_id, :integer
  end
end