require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Invalid' do
    before do
      @user = User.new
    end

    it "should be invalid" do
      @user.should_not be_valid
    end
  end
end
