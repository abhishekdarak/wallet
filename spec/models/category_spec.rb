require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'Invalid' do
    before do
      @category = Category.new
    end

    it "should be invalid" do
      @category.should_not be_valid
    end
  end
end
