require 'rails_helper'

RSpec.describe Income, type: :model do
  describe 'Invalid' do
    before do
      @income = Income.new
    end

    it "should be invalid" do
      @income.should_not be_valid
    end
  end
end
